import socket


class IPEndpoint(object):
	def __init__(self, address, port):
		"""
		Wraps an IPv4 address and port.
		
		:param address: Address of the endpoint
		:type address: str
		:param port: Port of the endpoint
		:type port: int
		"""
		if not IPEndpoint.check_address_format(address):
			raise ValueError("address must be a valid IPv4 address.")

		if not IPEndpoint.check_port_format(port):
			raise ValueError("port must be a valid port (between 0 and 65535).")

		self.__address = address
		self.__port = port

	@classmethod
	def check_address_format(cls, address):
		"""
		Checks if an address is a valid IPv4 address.
		
		:param address: Address to check
		:type address: str
		:return: True if the received address is valid. Otherwise, False.
		:rtype: bool
		"""
		try:
			socket.inet_aton(address)
			return True
		except(socket.error, TypeError):
			return False

	@classmethod
	def check_port_format(cls, port):
		"""
		Checks if an port number is valid.

		:param port: Port to check
		:type port: int
		:return: True if the received port is valid. Otherwise, False.
		:rtype: bool
		"""
		return not (type(port) != int or port < 0 or port >= 65536)

	@property
	def address(self):
		"""
		Gets the address of the endpoint
		
		:rtype: str
		"""
		return self.__address

	@property
	def port(self):
		"""
		Gets the port of the endpoint
		
		:rtype: int
		"""
		return self.__port
