class OscSubscription(object):
    def __init__(self, address, callback):
        """

        :param address:
        :type address: str
        :param callback: 
        :type callback: function[Message, tuple]
        """
        self.__address = address
        self.__callback = callback

    @property
    def address(self):
        """:rtype: str"""
        return self.__address

    @property
    def callback(self):
        """:rtype: function[Message, tuple]"""
        return self.__callback
