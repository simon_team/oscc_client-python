#!/usr/bin/env python
from twisted.internet import reactor
from txosc import dispatch
from txosc import osc
from txosc import async
from msvcrt import getch
from ipaddress import ip_address
from time import time
from random import random


# TODO Tornar o programa capaz de criar N clientes (testar se uma publicacao ja existe).
class ClientExampleV1(object):
    """
    Example that sends UDP messages.
    """

    def __init__(self, port, host="127.0.0.1"):
        self.port = port
        self.host = host

        self.client = async.DatagramClientProtocol()
        self._client_port = reactor.listenUDP(0, self.client)

        receiver = dispatch.Receiver()
        receiver.fallback = self.client_data_receive
        self._server_port = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        print "Listening on port %s" % self._server_port.getHost().port
        self.print_sending_to()

        self.initialize_state()

        self.client_delay_mult = 5

        reactor.callLater(0.1, self.ui_decision)

    def initialize_state(self):
        self.publisher_id = None
        self.subscriber_id = None

    def ui_decision(self, *args):
        print("-------- Actions ----------")
        print("|ESC: Exit                |")
        print("|  1: Add Publication     |")
        print("|  2: Remove Publication  |")
        print("|  3: Remove Publisher    |")
        print("|  4: Get Publications    |")
        print("|  5: Add Subscription    |")
        print("|  6: Remove Subscription |")
        print("|  7: Remove Subscriber   |")
        print("|  8: Start Client 1      |")
        print("|  9: Start Client 2      |")
        print("|  0: Reset               |")
        print("|  H: Set Host            |")
        print("|  P: Set Port            |")
        print("---------------------------")

        function_to_call = None
        while function_to_call is None:
            key = ord(getch())
            if key == 27:  # ESC
                function_to_call = reactor.stop
                print "Goodbye..."
            elif key == 49:  # 1
                function_to_call = self.send_add_publication
            elif key == 50:  # 2
                function_to_call = self.send_remove_publication
            elif key == 51:  # 3
                function_to_call = self.send_remove_publisher
            elif key == 52:  # 4
                function_to_call = self.send_get_publications
            elif key == 53:  # 5
                function_to_call = self.send_add_subscription
            elif key == 54:  # 6
                function_to_call = self.send_remove_subscription
            elif key == 55:  # 7
                function_to_call = self.send_remove_subscriber
            elif key == 56:  # 8
                function_to_call = self.start_client_1
            elif key == 57:  # 9
                function_to_call = self.start_client_2
            elif key == 48:  # 0
                self.initialize_state()
                print "INFO: Reset done"
            elif key == 104:  # H
                function_to_call = self.ask_address
            elif key == 112:  # P
                function_to_call = self.ask_port
            else:
                print "ERROR: Invalid key"
        function_to_call()

    def ask_address(self):
        address = None
        while address is None:
            address = raw_input('Enter the new target IP address: ')
            if len(address) == 0:
                return
            else:
                try:
                    ip_address(address.decode('unicode-escape'))
                except:
                    print "ERROR: Invalid address"
                    continue
                self.host = address
                print "INFO: Address changed"
                self.print_sending_to()
        reactor.callLater(0.1, self.ui_decision)

    def ask_port(self):
        port = None
        while port is None:
            try:
                port = int(raw_input('Enter the new target network port: '))
            except (ValueError):
                print "ERROR: Invalid port"
                continue
            type(int(port))
            self.port = port
            print "INFO: Por changed"
            self.print_sending_to()
        reactor.callLater(0.1, self.ui_decision)

    def print_sending_to(self):
        print("Sending to osc.udp://%s:%s from %s" % (self.host, self.port, self._client_port.getHost().port))

    def _send(self, element):
        # This method is defined only to simplify the example
        self.client.send(element, (self.host, self.port))
        print("Sent %s to %s:%d" % (element, self.host, self.port))

    # region Add Publication
    def send_add_publication(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_add_publication_response
        self.add_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/AddPublication", self.publisher_id, self.add_server.getHost().port,
                               "/Publicacao_1", "/Publicacao_1", "/Publicacao_3", "/Publicacao_4"))

    def print_add_publication_response(self, message, address):
        print("Add Publication Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            self.publisher_id = message.arguments[1].value

        defered = self.add_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Remove Publication
    def send_remove_publication(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_remove_publication_response
        self.remove_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/RemovePublication", self.publisher_id, self.remove_server.getHost().port,
                               "/Publicacao_2", "/Publicacao_3"))

    def print_remove_publication_response(self, message, address):
        print("Remove Publication Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            if message.arguments[0].value:
                self.publisher_id = None

        defered = self.remove_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Remove Publisher
    def send_remove_publisher(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_remove_publisher_response
        self.remove_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/RemovePublication", self.publisher_id, self.remove_server.getHost().port))

    def print_remove_publisher_response(self, message, address):
        print("Remove Publisher Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            if message.arguments[0].value:
                self.publisher_id = None

        defered = self.remove_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Get Publications
    def send_get_publications(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_get_publication_response
        self.get_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/GetPublications", self.get_server.getHost().port))

    def print_get_publication_response(self, message, address):
        print("Get Publication Response")
        print("  Got %s from %s" % (message, address))

        defered = self.get_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Add Subscription
    def send_add_subscription(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_add_subscription_response
        self.add_subscription_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/AddSubscription", self.subscriber_id,
                               self.add_subscription_server.getHost().port,
                               "/Publicacao_1", self._server_port.getHost().port,
                               "/Publicacao_1", self._server_port.getHost().port,
                               "/Publicacao_3", self._server_port.getHost().port,
                               "/Publicacao_4", self._server_port.getHost().port))

    def print_add_subscription_response(self, message, address):
        print("Add Subscription Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            was_registered = False
            for idx in range(1, len(message.arguments)):
                was_registered |= message.arguments[idx].value
            if was_registered:
                self.subscriber_id = message.arguments[0].value

        defered = self.add_subscription_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Remove Subscription
    def send_remove_subscription(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_remove_subscription_response
        self.remove_subscription_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/RemoveSubscription", self.subscriber_id,
                               self.remove_subscription_server.getHost().port,
                               "/Publicacao_2", "/Publicacao_3", "/Publicacao_1", "/Publicacao_3", "/Publicacao_4"))

    def print_remove_subscription_response(self, message, address):
        print("Remove Subscription Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            if message.arguments[0].value:
                self.subscriber_id = None

        defered = self.remove_subscription_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Remove Subscriber
    def send_remove_subscriber(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.print_remove_subscriber_response
        self.remove_subscription_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        self._send(osc.Message("/SIMON/OSCC/RemoveSubscription", self.subscriber_id,
                               self.remove_subscription_server.getHost().port))

    def print_remove_subscriber_response(self, message, address):
        print("Remove Subscriber Response")
        print("  Got %s from %s" % (message, address))

        if not message.address.endswith("Error"):
            if message.arguments[0].value:
                self.subscriber_id = None

        defered = self.remove_subscription_server.stopListening()
        defered.addCallback(self.ui_decision)

    # endregion

    # region Client
    def start_client_1(self):
        self.is_client_1 = True
        self.start_client_add_publication()

    def start_client_2(self):
        self.is_client_1 = False
        self.start_client_add_publication()

    def start_client_add_publication(self, *args):
        print "Adding publications..."
        self.client_add_publication()

    def start_client_add_subscription(self, *args):
        print "Publications added. Press any key to add subscriptions..."
        getch()
        print "Adding subscriptions..."
        self.client_add_subscription()

    def start_client_start_stream(self, *args):
        print "Subscriptions added. Press any key to start streaming data..."
        getch()
        print "Starting data stream..."
        self.client_start_streaming()

    def client_add_publication(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.client_add_publication_response
        self.client_add_publication_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        if self.is_client_1:
            self._send(osc.Message("/SIMON/OSCC/AddPublication", self.publisher_id,
                                   self.client_add_publication_server.getHost().port,
                                   "/Publicacao_1", "/Publicacao_2"))
        else:
            self._send(osc.Message("/SIMON/OSCC/AddPublication", self.publisher_id,
                                   self.client_add_publication_server.getHost().port,
                                   "/Publicacao_3", "/Publicacao_4"))

    def client_add_publication_response(self, message, address):
        print("Add Publication Response")
        print("  Got %s from %s" % (message, address))

        defered = self.client_add_publication_server.stopListening()
        if not message.address.endswith("Error"):
            self.publisher_id = message.arguments[1].value
            defered.addCallback(self.start_client_add_subscription)
        else:
            defered.addCallback(self.ui_decision)

    def client_add_subscription(self):
        receiver = dispatch.Receiver()
        receiver.fallback = self.client_add_subscription_response
        self.client_add_subscription_server = reactor.listenUDP(0, async.DatagramServerProtocol(receiver))

        if self.is_client_1:
            self._send(osc.Message("/SIMON/OSCC/AddSubscription", self.subscriber_id,
                                   self.client_add_subscription_server.getHost().port,
                                   "/Publicacao_3", self._server_port.getHost().port,
                                   "/Publicacao_4", self._server_port.getHost().port))
        else:
            self._send(osc.Message("/SIMON/OSCC/AddSubscription", self.subscriber_id,
                                   self.client_add_subscription_server.getHost().port,
                                   "/Publicacao_1", self._server_port.getHost().port,
                                   "/Publicacao_2", self._server_port.getHost().port))

    def client_add_subscription_response(self, message, address):
        print("Add Subscription Response")
        print("  Got %s from %s" % (message, address))

        defered = self.client_add_subscription_server.stopListening()

        if not message.address.endswith("Error"):
            was_registered = False
            for idx in range(1, len(message.arguments)):
                was_registered |= message.arguments[idx].value
            if was_registered:
                self.subscriber_id = message.arguments[0].value
                defered.addCallback(self.start_client_start_stream)
            else:
                defered.addCallback(self.ui_decision)
        else:
            defered.addCallback(self.ui_decision)

    def client_start_streaming(self):
        if self.is_client_1:
            reactor.callLater(0.1 * self.client_delay_mult, self.publication_1_data_stream)
            reactor.callLater(0.2 * self.client_delay_mult, self.publication_2_data_stream)
        else:
            reactor.callLater(0.3 * self.client_delay_mult, self.publication_3_data_stream)
            reactor.callLater(0.4 * self.client_delay_mult, self.publication_4_data_stream)

    def publication_1_data_stream(self):
        self._send(osc.Message("/Publicacao_1", "Publicacao_1", 0.1 * self.client_delay_mult, 1 + random(), time()))
        reactor.callLater(0.1 * self.client_delay_mult, self.publication_1_data_stream)

    def publication_2_data_stream(self):
        self._send(osc.Message("/Publicacao_2", "Publicacao_2", 0.2 * self.client_delay_mult, 2 + random(), time()))
        reactor.callLater(0.2 * self.client_delay_mult, self.publication_2_data_stream)

    def publication_3_data_stream(self):
        self._send(osc.Message("/Publicacao_3", "Publicacao_3", 0.3 * self.client_delay_mult, 3 + random(), time()))
        reactor.callLater(0.3 * self.client_delay_mult, self.publication_3_data_stream)

    def publication_4_data_stream(self):
        self._send(osc.Message("/Publicacao_4", "Publicacao_4", 0.4 * self.client_delay_mult, 4 + random(), time()))
        reactor.callLater(0.4 * self.client_delay_mult, self.publication_4_data_stream)

    def client_data_receive(self, message, address):
        print("Data Receive")
        print("  Got %s from %s" % (message, address))

    # endregion

    pass


if __name__ == "__main__":
    app = ClientExampleV1(5103)
    reactor.run()
