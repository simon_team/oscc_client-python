from time import sleep, time

from osc_subscription import OscSubscription
from oscc_client import OsccClient
from twisted.internet import reactor
from txosc.osc import Message

from OLD.ip_endpoint import IPEndpoint

oscc_client = OsccClient(IPEndpoint("127.0.0.1", 5103))


def add_publication_callback(result, arguments):
    """

    :param result:
    :type result: bool
    :param arguments:
    :type arguments: tuple
    :return: 
    """
    print "add_publication", result, [argument.value for argument in arguments]
    reactor.callLater(.1, call_later_publish_callback)


def remove_publication_callback(result, arguments):
    """

    :param result:
    :type result: bool
    :param arguments:
    :type arguments: tuple
    :return: 
    """
    print "remove_publication", result, [argument.value for argument in arguments]
    reactor.stop()


def add_subscription_callback(result, arguments):
    """

    :param result:
    :type result: bool
    :param arguments:
    :type arguments: tuple
    :return: 
    """
    print "add_subscription", result, [argument.value for argument in arguments]
    reactor.callLater(.1, call_later_remove_subscription_callback)


def remove_subscription_callback(result, arguments):
    """

    :param result:
    :type result: bool
    :param arguments:
    :type arguments: tuple
    :return: 
    """
    print "remove_subscription", result, [argument.value for argument in arguments]
    reactor.stop()


def subscription_callback(message, address):
    print message, address


def get_publications_callback(result, arguments):
    """:type arguments: tuple"""
    print "get_publications", [argument.value for argument in arguments]
    reactor.stop()


def call_later_add_publication_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    oscc_client.add_publication(["/Publicacao_1", "/Publicacao_2", "/Publicacao_1"], add_publication_callback)


def call_later_remove_publication_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    oscc_client.remove_publication(["/Publicacao_1", "/Publicacao_2", "/Publicacao_1"],
                                   remove_publication_callback)


def call_later_add_subscription_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    subscriptions = [OscSubscription("/Publicacao_1", subscription_callback),
                     OscSubscription("/Publicacao_2", subscription_callback),
                     OscSubscription("/Publicacao_1", subscription_callback)]
    oscc_client.add_subscription(add_subscription_callback, *subscriptions)


def call_later_remove_subscription_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    oscc_client.remove_subscription(["/Publicacao_1", "/Publicacao_1"],
                                    remove_subscription_callback)


def call_later_get_publications_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    oscc_client.get_publications(get_publications_callback)


def call_later_publish_callback():
    while not oscc_client.can_send_request:
        sleep(0.1)
    oscc_client.publish(Message("/Publicacao_1", "Test", time()))


reactor.callLater(.1, call_later_add_publication_callback)
reactor.run()
