from twisted.internet import reactor

from txosc.async import DatagramClientProtocol, DatagramServerProtocol
from txosc.dispatch import Receiver
from txosc.osc import Message

from ip_endpoint import IPEndpoint
from client_state import ClientState
from osc_subscription import OscSubscription


# TODO Usar deferred em vez de funcoes callback
# TODO Rever returns
class OsccClient(object):
    # region Address Constants
    OSCC_ADDRESS = "/SIMON/OSCC"
    ADD_PUBLICATION_ADDRESS = OSCC_ADDRESS + "/AddPublication"
    REMOVE_PUBLICATION_ADDRESS = OSCC_ADDRESS + "/RemovePublication"
    ADD_SUBSCRIPTION_ADDRESS = OSCC_ADDRESS + "/AddSubscription"
    REMOVE_SUBSCRIPTION_ADDRESS = OSCC_ADDRESS + "/RemoveSubscription"
    GET_PUBLICATIONS_ADDRESS = OSCC_ADDRESS + "/GetPublications"
    RESPONSE_ADDRESS = OSCC_ADDRESS + "/Response"
    ERROR_ADDRESS = OSCC_ADDRESS + "/Error"
    # endregion

    RESPONSE_TIMEOUT = 5

    def __init__(self, oscc_endpoint):
        """
        
        :param oscc_endpoint:
        :type oscc_endpoint: IPEndpoint
        """
        self.__oscc_endpoint = oscc_endpoint

        self.__client = DatagramClientProtocol()
        self.__client_port = reactor.listenUDP(0, self.__client).getHost().port

        self.__server = DatagramServerProtocol(Receiver())
        self.__server.receiver.fallback = self.__fallback
        self.__server_port = reactor.listenUDP(0, self.__server).getHost().port

        self.__client_state = None
        """:type __current_state: ClientState"""

        self.__publication_port = -1

        self.__publisher_id = None
        """:type __publisher_id: str"""
        self.__publication_addresses = []
        self.__subscriber_id = None
        """:type __subscriber_id: str"""
        self.__subscription_addresses = []
        self.reset_status()

    # region Properties
    @property
    def oscc_endpoint(self):
        return self.__oscc_endpoint

    @property
    def client_port(self):
        return self.__client_port

    @property
    def server_port(self):
        return self.__server_port

    @property
    def publisher_id(self):
        return self.__publisher_id

    @property
    def publication_addresses(self):
        return tuple(self.__publication_addresses)

    @property
    def publication_count(self):
        return len(self.__publication_addresses)

    @property
    def subscriber_id(self):
        return self.__subscriber_id

    @property
    def subscription_addresses(self):
        return tuple(self.__subscription_addresses)

    @property
    def subscription_count(self):
        return len(self.__subscription_addresses)

    @property
    def can_send_request(self):
        return self.__client_state is None

    # endregion

    def reset_status(self):
        self.__publisher_id = None
        self.__publication_addresses = []
        self.__subscriber_id = None
        self.__subscription_addresses = []

    def __send_message(self, message):
        # This method is defined only to simplify the example
        self.__client.send(message, (self.__oscc_endpoint.address, self.__oscc_endpoint.port))
        print("Sent %s to %s:%d" % (message, self.__oscc_endpoint.address, self.__oscc_endpoint.port))

    def __generic_response_callback(self, message, address, callback):
        if self.__client_state is None:
            raise ValueError("There is no ClientState defined")

        if message.address == self.RESPONSE_ADDRESS:
            callback(message, address)
        elif message.address == self.ERROR_ADDRESS:
            self.__client_state.callback(False, message.arguments)
        else:
            self.__fallback(message, address)

    def __reset_receiver_state(self):
        self.__remove_callbacks_by_address(self.RESPONSE_ADDRESS)
        self.__remove_callbacks_by_address(self.ERROR_ADDRESS)

        if self.__client_state is not None:
            self.__client_state.timeout_callback.cancel()
            self.__client_state = None

    def __remove_callbacks_by_address(self, address):
        """:type address: str"""
        callbacks = tuple(self.__server.receiver.getCallbacks(address))
        for callback in callbacks:
            self.__server.receiver.removeCallback(address, callback)

    def __timeout_callback(self):
        self.__client_state.callback(False, [])
        self.__client_state = None

    @staticmethod
    def __fallback(message, address):
        """If no callback is registered for the received OSC address, then this function is called.

        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        print "Got unknown message ({0}) from {2}:{3}.\n    Message: {1}".format(message.address, message, *address)

    # region AddPublication
    def add_publication(self, publication_address, callback_function):
        """Sends to the server a request to add the received publication addresses.

        The callback function must receive a boolean, which indicates whether the request was successful or not,
        and a list, which has the arguments returned by the server.

        NOTE: Only one order can be sent at a time. Only after the server's response can a new message be sent.

        :param publication_address: Address (or a set of addresses) to add.
        :type publication_address: str or list or tuple
        :param callback_function: Callback function that will be called when the server's response arrive.
        :type callback_function: function
        :return: True if the message was sent. Otherwise, False.
        :rtype: bool
        """
        # TODO Verificar formatacao do endereco

        if self.__client_state is not None:
            return False

        if type(publication_address) == str:
            publication_address = [publication_address]
        elif type(publication_address) != list and type(publication_address) != tuple:
            return False
        else:
            for address in publication_address:
                if type(address) != str or address in self.__publication_addresses:
                    return False

        self.__server.receiver.addCallback(self.RESPONSE_ADDRESS, self.__add_publication_callback)
        self.__server.receiver.addCallback(self.ERROR_ADDRESS, self.__add_publication_callback)

        self.__send_message(Message(
            self.ADD_PUBLICATION_ADDRESS, self.__publisher_id, self.__server_port, *publication_address))

        self.__client_state = ClientState(callback_function, publication_address,
                                          reactor.callLater(self.RESPONSE_TIMEOUT, self.__timeout_callback))

        return True

    def __add_publication_callback(self, message, address):
        """Callback that handles the response of a AddPublication request.
        
        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        self.__generic_response_callback(message, address, self.__add_publication_success_callback)
        self.__reset_receiver_state()

    def __add_publication_success_callback(self, message, address):
        # TODO Associar o porto retornado pelo servidor ao endereco OSC em questao
        self.__publication_port = message.arguments[0].value
        if (type(self.__publication_port) != int or len(self.__client_state.data) != (len(message.arguments) - 2)):
            self.__publication_port = -1
            self.__fallback(message, address)
            return

        self.__publisher_id = message.arguments[1].value
        if type(self.__publisher_id) != str:
            self.__publisher_id = None
            self.__fallback(message, address)
            return

        for idx in range(len(self.__client_state.data)):
            if message.arguments[idx + 2].value:
                self.__publication_addresses.append(self.__client_state.data[idx])

        self.__client_state.callback(True, message.arguments[2:])

    # endregion

    # region RemovePublication
    def remove_publication(self, publication_address, callback_function):
        # TODO Alterar referencia a callback function
        """Sends to the server a request to remove the received publication addresses.
        
        The callback function must receive a boolean, which indicates whether the request was successful or not,
        and a list, which has the arguments returned by the server.
        
        NOTE: Only one order can be sent at a time. Only after the server's response can a new message be sent.
        
        :param publication_address: Address (or a set of addresses) to remove.
        :type publication_address: str or list or tuple
        :param callback_function: Callback function that will be called when the server's response arrive.
        :type callback_function: function
        :return: True if the message was sent. Otherwise, False.
        :rtype: bool
        """
        # TODO Verificar formatacao do endereco

        if self.__client_state is not None or self.__publisher_id is None:
            return False

        if type(publication_address) == str:
            publication_address = [publication_address]
        elif type(publication_address) != list and type(publication_address) != tuple:
            return False
        else:
            for address in publication_address:
                if type(address) != str or address not in self.__publication_addresses:
                    return False

        self.__server.receiver.addCallback(self.RESPONSE_ADDRESS, self.__remove_publication_callback)
        self.__server.receiver.addCallback(self.ERROR_ADDRESS, self.__remove_publication_callback)

        self.__send_message(Message(
            self.REMOVE_PUBLICATION_ADDRESS, self.__publisher_id, self.__server_port, *publication_address))

        self.__client_state = ClientState(callback_function, publication_address,
                                          reactor.callLater(self.RESPONSE_TIMEOUT, self.__timeout_callback))

    def __remove_publication_callback(self, message, address):
        """Callback that handles the response of a RemovePublication request.

        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        self.__generic_response_callback(message, address, self.__remove_publication_success_callback)
        self.__reset_receiver_state()

    def __remove_publication_success_callback(self, message, address):
        publisher_removed = message.arguments[0].value
        if (type(publisher_removed) != bool or
                (len(self.__client_state.data) != (len(message.arguments) - 1) and len(message.arguments) != 1)):
            self.__fallback(message, address)
            return

        for idx in range(len(self.__client_state.data)):
            if message.arguments[idx + 1].value:
                self.__publication_addresses.remove(self.__client_state.data[idx])

        self.__client_state.callback(True, message.arguments[1:])

    # endregion

    # region Publish
    def publish(self, publication):
        """

        :param publication:
        :type publication: Message
        :return:
        :rtype: bool
        """

        if (self.__client_state is not None or self.__publisher_id is None or
                    publication.address not in self.__publication_addresses):
            return False

        self.__send_message(publication)
        return True

    # endregion

    # region GetPublications
    def get_publications(self, callback_function):
        """Sends to the server a request to get the available publication addresses.
        
        The callback function must receive a list, which has the arguments returned by the server.
        
        NOTE: Only one order can be sent at a time. Only after the server's response can a new message be sent.
        
        :param callback_function: Callback function that will be called when the server's response arrive.
        :type callback_function: function
        :return: True if the message was sent. Otherwise, False.
        :rtype: bool
        """
        # TODO Verificar formatacao do endereco

        if self.__client_state is not None:
            return False

        self.__server.receiver.addCallback(self.RESPONSE_ADDRESS, self.__get_publications_callback)
        self.__server.receiver.addCallback(self.ERROR_ADDRESS, self.__get_publications_callback)

        self.__send_message(Message(self.GET_PUBLICATIONS_ADDRESS, self.__server_port))

        self.__client_state = ClientState(callback_function, [],
                                          reactor.callLater(self.RESPONSE_TIMEOUT, self.__timeout_callback))

        return True

    def __get_publications_callback(self, message, address):
        """Callback that handles the response of a GetPublications request.

        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        self.__generic_response_callback(message, address, self.__get_publications_success_callback)
        self.__reset_receiver_state()

    def __get_publications_success_callback(self, message, address):
        self.__client_state.callback(True, message.arguments)

    # endregion

    # region AddSubscription
    def add_subscription(self, callback_function, *subscriptions):
        """Sends to the server a request to subscribe to the received subscription addresses.
        
        The callback functions must receive a boolean, which indicates whether the request was successful or not,
        and a list, which has the arguments returned by the server.        
        
        NOTE: Only one order can be sent at a time. Only after the server's response can a new message be sent.
        
        :param callback_function: Callback function that will be called when the server's response arrive.
        :type callback_function: function
        :param subscriptions: Address (or a set of addresses) to subscribe.
        :type subscriptions: osc_subscription
        :return: True if the message was sent. Otherwise, False.
        :rtype: bool
        """
        # TODO Verificar formatacao do endereco

        if self.__client_state is not None:
            return False

        if type(subscriptions) != list and type(subscriptions) != tuple:
            return False
        else:
            for subscription in subscriptions:
                if type(subscription) != OscSubscription or subscription.address in self.__subscription_addresses:
                    return False

        subscription_port_pairs = [(subscription.address, self.__server_port) for subscription in subscriptions]
        subscription_port_pairs = [item for sublist in subscription_port_pairs for item in sublist]

        self.__server.receiver.addCallback(self.RESPONSE_ADDRESS, self.__add_subscription_callback)
        self.__server.receiver.addCallback(self.ERROR_ADDRESS, self.__add_subscription_callback)

        self.__send_message(Message(
            self.ADD_SUBSCRIPTION_ADDRESS, self.__subscriber_id, self.__server_port, *subscription_port_pairs))

        self.__client_state = ClientState(callback_function, subscriptions,
                                          reactor.callLater(self.RESPONSE_TIMEOUT, self.__timeout_callback))

        return True

    def __add_subscription_callback(self, message, address):
        """Callback that handles the response of a AddSubscription request.

        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        self.__generic_response_callback(message, address, self.__add_subscription_success_callback)
        self.__reset_receiver_state()

    def __add_subscription_success_callback(self, message, address):
        if len(self.__client_state.data) != (len(message.arguments) - 1):
            self.__fallback(message, address)
            return

        self.__subscriber_id = message.arguments[0].value
        if type(self.__subscriber_id) != str:
            self.__subscriber_id = None
            self.__fallback(message, address)
            return

        for idx in range(len(self.__client_state.data)):
            current_address = self.__client_state.data[idx].address
            if message.arguments[idx + 1].value:
                self.__subscription_addresses.append(current_address)
                self.__server.receiver.addCallback(current_address, self.__client_state.data[idx].callback)

        self.__client_state.callback(True, message.arguments[1:])

    # endregion

    # region RemoveSubscription
    def remove_subscription(self, subscription_address, callback_function):
        """Sends to the server a request to remove the received subscription addresses.

        The callback function must receive a boolean, which indicates whether the request was successful or not,
        and a list, which has the arguments returned by the server.

        NOTE: Only one order can be sent at a time. Only after the server's response can a new message be sent.

        :param subscription_address: Address (or a set of addresses) to remove.
        :type subscription_address: str or list or tuple
        :param callback_function: Callback function that will be called when the server's response arrive.
        :type callback_function: function
        :return: True if the message was sent. Otherwise, False.
        :rtype: bool
        """
        # TODO Verificar formatacao do endereco

        if self.__client_state is not None or self.__subscriber_id is None:
            return False

        if type(subscription_address) == str:
            subscription_address = [subscription_address]
        elif type(subscription_address) != list and type(subscription_address) != tuple:
            return False
        else:
            for address in subscription_address:
                if type(address) != str or address not in self.__subscription_addresses:
                    return False

        self.__server.receiver.addCallback(self.RESPONSE_ADDRESS, self.__remove_subscription_callback)
        self.__server.receiver.addCallback(self.ERROR_ADDRESS, self.__remove_subscription_callback)

        self.__send_message(Message(
            self.REMOVE_SUBSCRIPTION_ADDRESS, self.__subscriber_id, self.__server_port, *subscription_address))

        self.__client_state = ClientState(callback_function, subscription_address,
                                          reactor.callLater(self.RESPONSE_TIMEOUT, self.__timeout_callback))

    def __remove_subscription_callback(self, message, address):
        """Callback that handles the response of a RemoveSubscription request.

        :param message: OSC Message received. 
        :type message: Message
        :param address: IP address where the message came from.
        :type address: (str, int)
        """
        self.__generic_response_callback(message, address, self.__remove_subscription_success_callback)
        self.__reset_receiver_state()

    def __remove_subscription_success_callback(self, message, address):
        subscriber_removed = message.arguments[0].value
        if (type(subscriber_removed) != bool or
                (len(self.__client_state.data) != (len(message.arguments) - 1) and len(message.arguments) != 1)):
            self.__fallback(message, address)
            return

        for idx in range(len(self.__client_state.data)):
            if message.arguments[idx + 1].value:
                subscription_address = self.__client_state.data[idx]
                self.__subscription_addresses.remove(subscription_address)
                self.__remove_callbacks_by_address(subscription_address)

        self.__client_state.callback(True, message.arguments[1:])

    # endregion

    pass
