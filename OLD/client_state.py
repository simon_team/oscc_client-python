class ClientState(object):
    def __init__(self, callback, data, timeout_callback):
        """

        :param callback:
        :type callback: function
        :param data:
        :type data: list or tuple or object
        :param timeout_callback:
        :type timeout_callback: function
        """
        if callback is None or data is None or timeout_callback is None:
            raise ValueError("None of the parameters can be None.")

        self.__callback = callback
        self.__data = data
        self.__timeout_callback = timeout_callback

    @property
    def callback(self):
        return self.__callback

    @property
    def data(self):
        return self.__data

    @property
    def timeout_callback(self):
        return self.__timeout_callback
