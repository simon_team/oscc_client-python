import colorama
import os

# Colorama color codes
# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL

colorama.init(strip=False)


class Color:
	def __init__(self):
		raise NotImplementedError()

	DARK_RED = colorama.Fore.RED + colorama.Style.DIM
	DARK_GREEN = colorama.Fore.RED + colorama.Style.DIM
	DARK_YELLOW = colorama.Fore.RED + colorama.Style.DIM
	DARK_BLUE = colorama.Fore.RED + colorama.Style.DIM
	DARK_MAGENTA = colorama.Fore.RED + colorama.Style.DIM
	DARK_CYAN = colorama.Fore.RED + colorama.Style.DIM

	LIGHT_RED = colorama.Fore.RED + colorama.Style.BRIGHT
	LIGHT_GREEN = colorama.Fore.RED + colorama.Style.BRIGHT
	LIGHT_YELLOW = colorama.Fore.RED + colorama.Style.BRIGHT
	LIGHT_BLUE = colorama.Fore.RED + colorama.Style.BRIGHT
	LIGHT_MAGENTA = colorama.Fore.RED + colorama.Style.BRIGHT
	LIGHT_CYAN = colorama.Fore.RED + colorama.Style.BRIGHT


def print_light_red(message):
	print(Color.LIGHT_RED + message + colorama.Style.RESET_ALL)


def print_dark_green(message):
	print(Color.DARK_GREEN + message + colorama.Style.RESET_ALL)


def print_dark_magenta(message):
	print(Color.DARK_MAGENTA + message + colorama.Style.RESET_ALL)


def begin_color(color, message):
	"""
	:type color: str
	:type message: str
	"""
	print(color + message)


def end_color(message):
	print(message + colorama.Style.RESET_ALL)


def cls():
	os.system('cls' if os.name == 'nt' else 'clear')
