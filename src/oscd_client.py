# -*- coding: utf-8 -*-

"""
OSC Distributor Client Module
"""

from uuid import UUID
from re import match as regex_match
from sys import stdout
from threading import Thread
from twisted.internet import reactor
from twisted.python import log
from twisted.internet.tcp import Connector
from twisted.python.failure import Failure
from twisted.internet.error import TimeoutError, ConnectionDone, ConnectionLost
from twisted.internet.defer import Deferred, DeferredList, gatherResults

from txosc.async import DatagramClientProtocol, DatagramServerProtocol, ClientFactory
from txosc.dispatch import Receiver
from txosc.osc import Message, OscError

from oscd_subscription import OscdSubscription


# TODO Implementar
class OscdClient:
    """
        OSC Distributor Client Class
    """

    # region OSCD Addresses
    REGISTER_USER_ADDRESS = "/OSCD/RegisterUser"
    REGISTER_SUBJECT_ADDRESS = "/OSCD/RegisterSubject"
    UNREGISTER_SUBJECT_ADDRESS = "/OSCD/UnregisterSubject"
    GET_AVAILABLE_SUBJECTS_ADDRESS = "/OSCD/GetAvailableSubjects"
    ENABLE_SUBJECT_NOTIFICATION_ADDRESS = "/OSCD/EnableSubjectNotification"
    DISABLE_SUBJECT_NOTIFICATION_ADDRESS = "/OSCD/DisableSubjectNotification"
    SUBSCRIBE_SUBJECT_ADDRESS = "/OSCD/SubscribeSubject"
    UNSUBSCRIBE_SUBJECT_ADDRESS = "/OSCD/UnsubscribeSubject"

    RESPONSE_ADDRESS = "/OSCD/Response"
    ERROR_ADDRESS = "/OSCD/Error"
    SUBJECT_CANCELLATION_ADDRESS = "/OSCD/SubjectCancellation"
    SUBJECT_NOTIFICATION_ADDRESS = "/OSCD/SubjectNotification"
    # endregion

    # Regex pattern that validate OSC address
    SUBJECT_REGEX_PATTERN = "^(\\/[A-Za-z0-9]+)+$"

    # Thread that runs the reactor loop
    __REACTOR_THREAD = None
    # Flag that indicates if the reactor already ran (it can be currently running)
    __REACTOR_ALREADY_RAN = False

    # region Initialization
    def __init__(self, address, port):
        """
        :param address: IP address of the OSC Distributor
        :type address: str
        :param port: IP port of the OSC Distributor
        :type port: int
        """
        self.__address = address
        self.__port = port

        # OSC user identification
        self.__osc_user_id = None

        # TCP client with which the client sends and receives commands
        self.__command_client = self.__initialize_command_client()
        # TCP connection used to send and receive commands
        self.__command_client_connector = None
        # UDP client with which the client sends and receives publications
        self.__publication_sender, self.__publication_receiver = self.__initialize_publication_client()
        # UDP port from where the client sends publications
        self.__publication_sender_listening_port = None
        # UDP port from where the client receives publications
        self.__publication_receiver_listening_port = None

        # Command message counter
        self.__command_counter = 0
        # Commands waiting for response
        self.__pending_commands = dict()

        # Active subjects
        self.__active_subjects = dict()
        # Subjects waiting for response
        self.__pending_subscriptions = dict()
        # Active subscriptions
        self.__active_subscriptions = dict()
        # Active notifications
        self.__active_notifications = set()

        # Callback function called when the connection request is completed
        self.__on_connection_complete = None
        # Callback function called when the connection is terminated involuntarily
        self.__on_disconnected = None
        # Callback function called when the connection is terminated by the user
        self.__on_disconnection_complete = None
        # Flag that indicates if the user wants to stop the reactor loop
        self.__stop_reactor_on_disconnection = False
        # Flag that indicates if the client is connected
        self.__is_connected = False

        # Callback function called when a subscribed subject is cancelled
        self.__on_subject_cancellation_received = None
        # Callback function called when a subject with notification is available
        self.__on_subject_notification_received = None

        # Callback function called when an user's callback function fails
        self.__on_callback_failure = None

    def __initialize_command_client(self):
        """
        :rtype: ClientFactory
        """
        receiver = Receiver()
        receiver.addCallback(self.RESPONSE_ADDRESS, self.__response_received)
        receiver.addCallback(self.ERROR_ADDRESS, self.__response_received)
        receiver.addCallback(self.SUBJECT_CANCELLATION_ADDRESS, self.__subject_cancellation_received)
        receiver.addCallback(self.SUBJECT_NOTIFICATION_ADDRESS, self.__subject_notification_received)
        receiver.setFallback(self.__command_fallback)

        client_factory = ClientFactory(receiver)
        client_factory.deferred.addCallback(self.__on_command_client_connection_complete)
        client_factory.clientConnectionFailed = self.__on_command_client_connection_complete
        client_factory.clientConnectionLost = self.__on_command_client_connection_lost

        return client_factory

    def __initialize_publication_client(self):
        """
        :rtype: (DatagramClientProtocol, DatagramServerProtocol)
        """
        receiver = Receiver()
        receiver.setFallback(self.__publication_fallback)

        return DatagramClientProtocol(), DatagramServerProtocol(receiver)

    def __reset_client(self):
        self.__osc_user_id = None
        self.__active_subjects.clear()
        self.__pending_subscriptions.clear()
        self.__active_subscriptions.clear()
        self.__active_notifications.clear()
    # endregion

    # region Getters/Setters
    def set_on_callback_failure(self, callback):
        """
        :type callback: (Failure) -> None
        """
        if callback is None or not callable(callback):
            raise ValueError("The received callback is not valid.")

        self.__on_callback_failure = callback

    def set_on_subject_cancellation_received(self, callback):
        """
        :type callback: (str) -> None
        """
        if callback is None or not callable(callback):
            raise ValueError("The received callback is not valid.")

        self.__on_subject_cancellation_received = callback

    def set_on_subject_notification_received(self, callback):
        """
        :type callback: (str) -> None
        """
        if callback is None or not callable(callback):
            raise ValueError("The received callback is not valid.")

        self.__on_subject_notification_received = callback

    def set_disconnected(self, callback):
        """
        :type callback: (str) -> None
        """
        if callback is None or not callable(callback):
            raise ValueError("The received callback is not valid.")

        self.__on_disconnected = callback

    # endregion

    # region Connection
    def connect(self, callback, start_reactor=False, connection_timeout=3):
        """
        :type callback: (bool) -> None
        :type start_reactor: bool
        """
        if self.__is_connected:
            raise RuntimeError("The client already is connected.")

        if not reactor.running and self.__REACTOR_ALREADY_RAN:
            raise RuntimeError("Can't reconnect because reactor has already run.")

        if not callable(callback):
            raise ValueError("The received callback is not valid.")

        if not isinstance(start_reactor, bool):
            raise ValueError("The parameter 'start_reactor' must be a boolean.")

        self.__command_client_connector = reactor.connectTCP(
            self.__address, self.__port, self.__command_client, timeout=connection_timeout)

        self.__publication_sender_listening_port = reactor.listenUDP(0, self.__publication_sender)
        self.__publication_receiver_listening_port = reactor.listenUDP(0, self.__publication_receiver)

        self.__on_connection_complete = callback

        if start_reactor and not reactor.running:
            log.startLogging(stdout)
            self.__REACTOR_THREAD = Thread(target=reactor.run, args=(False,), name="Reactor")
            self.__REACTOR_THREAD.start()
            self.__REACTOR_ALREADY_RAN = True

    def disconnect(self, callback, stop_reactor=False):
        """
        :type callback: () -> None
        :type stop_reactor: bool
        """
        if not self.__is_connected:
            raise RuntimeError("The client is not connected.")

        if not callable(callback):
            raise ValueError("The received callback is not valid.")

        if not isinstance(stop_reactor, bool):
            raise ValueError("The parameter 'stop_reactor' must be a boolean.")

        self.__command_client_connector.disconnect()
        self.__publication_sender_listening_port.stopListening()
        self.__publication_receiver_listening_port.stopListening()
        self.__reset_client()

        self.__stop_reactor_on_disconnection = stop_reactor
        self.__on_disconnection_complete = callback
        # TODO Perceber melhor o gather e tentar tirar partido da coisa

        # defer.gatherResults

        return True

    @property
    def is_connected(self):
        """
        :rtype: bool
        """
        return self.__is_connected

    def wait_for_reactor_stop(self):
        """
        Should only be called after calling the function 'disconnect' with the flag 'stop_reactor' set to True
        """
        self.__REACTOR_THREAD.join()

    def __on_command_client_connection_complete(self, result, reason=None):
        """
        :type result: Connector | bool
        :type reason: Failure
        :rtype: None | Failure
        """
        callback = self.__on_connection_complete
        self.__on_connection_complete = None
        self.__is_connected = False

        if reason is None:
            self.__is_connected = result
            self.__call_callback(callback, result)
        elif reason.type is TimeoutError:
            self.__call_callback(callback, False)
        else:
            log.err(reason)
            return reason

    def __on_command_client_connection_lost(self, connector, reason):
        """
        :type connector: Connector
        :type reason: Failure
        :rtype: None | Failure
        """
        callback = self.__on_disconnection_complete
        self.__on_disconnection_complete = None
        self.__is_connected = False

        if reason.type is ConnectionDone:
            self.__call_callback(callback, None)
        elif reason.type is ConnectionLost and callable(self.__on_disconnected):
            self.__call_callback(self.__on_disconnected, None)
        else:
            log.err(reason)
            return reason

        if self.__stop_reactor_on_disconnection and reactor.running:
            reactor.stop()
        self.__stop_reactor_on_disconnection = False

    # endregion

    # region Command Senders
    def __get_new_command_message(self, address, append_user_id=True):
        """
        :type address: str
        :type append_user_id: bool
        :rtype: Message
        """
        self.__command_counter += 1
        message = Message(address, self.__command_counter)

        if append_user_id:
            message.add(self.__osc_user_id)

        return message

    def __send_command(self, command, command_handler=None):
        """
        :type command: Message
        :type command_handler: (Any) -> None
        :rtype: bool
        """
        if not self.is_connected:
            return False

        if command_handler is not None:
            command_handler_data = (command.arguments[0].value, command, command_handler)
            self.__command_client.send(command)
            self.__pending_commands[command_handler_data[0]] = command_handler_data

            log.msg("Command sent with the id", command_handler_data[0])
        else:
            self.__command_client.send(command)

            log.msg("Command sent.")

        return True

    def register_user(self, handler):
        """
        :type handler: (bool) -> None
        :rtype: bool
        """
        # Tests if the client is registered or if there already is a similar pending command
        if self.__osc_user_id is not None or self.__has_register_user_command_pending():
            return False

        # Tests if the received handler is a function
        if not callable(handler):
            raise ValueError("The received handler is not valid.")

        # Creates and sends the command
        return self.__send_command(self.__get_new_command_message(self.REGISTER_USER_ADDRESS, False), handler)

    def register_subject(self, handler, *subjects):
        """
        :type handler: (dict[str, bool]) -> None
        :type subjects: str
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Tests if the received handler is a function
        if not callable(handler):
            raise ValueError("The received handler is not valid.")

        # Filters the received subjects
        subjects_set = self.__get_filtered_subjects(subjects, self.REGISTER_SUBJECT_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.REGISTER_SUBJECT_ADDRESS)
        for subject in subjects_set:
            message.add(subject)

        return self.__send_command(message, handler)

    def unregister_subject(self, *subjects):
        """
        :type subjects: str
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Gets a set of non-repetitive and valid subjects
        subjects_set = self.__get_subject_set(subjects)

        # Gets a set of non-repetitive and valid subjects
        subjects_set.intersection_update(self.__active_subjects.keys())

        # Gets a set of non-repetitive and valid subjects
        self.__filter_subjects_by_pending_command(subjects_set, self.UNREGISTER_SUBJECT_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.UNREGISTER_SUBJECT_ADDRESS)
        for subject in subjects_set:
            self.__active_subjects.pop(subject)
            message.add(subject)

        return self.__send_command(message)

    def get_available_subjects(self, handler):
        """
        :type handler: (list[str]) -> None
        :rtype: bool
        """
        # Tests if the received handler is a function
        if not callable(handler):
            raise ValueError("The received handler is not valid.")

        # Tests if there already is a similar pending command
        for command in self.__pending_commands.values():
            if command[1].address == self.GET_AVAILABLE_SUBJECTS_ADDRESS:
                return False

        # Creates and sends the command
        return self.__send_command(self.__get_new_command_message(self.GET_AVAILABLE_SUBJECTS_ADDRESS, False), handler)

    def enable_subject_notification(self, *subjects):
        """
        :type subjects: str
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Filters the received subjects
        subjects_set = self.__get_filtered_subjects(subjects, self.ENABLE_SUBJECT_NOTIFICATION_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.ENABLE_SUBJECT_NOTIFICATION_ADDRESS)
        for subject in subjects:
            self.__active_notifications.add(subject)
            message.add(subject)

        return self.__send_command(message)

    def disable_subject_notification(self, *subjects):
        """
        :type subjects: str
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Gets a set of non-repetitive and valid subjects
        subjects_set = self.__get_subject_set(subjects)

        # Excludes the received subjects that aren't activeNotifications
        subjects_set.intersection_update(self.__active_notifications)

        # Excludes the received subjects that are in pending commands
        self.__filter_subjects_by_pending_command(subjects_set, self.DISABLE_SUBJECT_NOTIFICATION_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.DISABLE_SUBJECT_NOTIFICATION_ADDRESS)
        for subject in subjects_set:
            self.__active_notifications.remove(subject)
            message.add(subject)

        return self.__send_command(message)

    def subscribe_subject(self, handler, *subscriptions):
        """
        :type handler: (dict[str, bool]) -> None
        :type subscriptions: OscdSubscription
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Tests if the received handler is a function
        if not callable(handler):
            raise ValueError("The received handler is not valid.")

        # Filters the received subjects
        subjects_set = self.__get_filtered_subjects([subscription.subject for subscription in subscriptions],
                                                    self.SUBSCRIBE_SUBJECT_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.SUBSCRIBE_SUBJECT_ADDRESS)
        for subject in subjects_set:
            subscription = next(subscription for subscription in subscriptions if subscription.subject == subject)
            self.__pending_subscriptions[subscription.subject] = subscription.handler

            message.add(subscription.subject)
            message.add(self.__publication_receiver_listening_port.getHost().port)
            message.add(subscription.enable_notification)

        return self.__send_command(message, handler)

    def unsubscribe_subject(self, *subjects):
        """
        :type subjects: str
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Gets a set of non-repetitive and valid subjects
        subjects_set = self.__get_subject_set(subjects)

        # Excludes the received subjects that aren't activeNotifications
        subjects_set.intersection_update(self.__active_subscriptions.keys())

        # Excludes the received subjects that are in pending commands
        self.__filter_subjects_by_pending_command(subjects_set, self.UNSUBSCRIBE_SUBJECT_ADDRESS)

        # Tests if there is any subject do register
        if len(subjects_set) == 0:
            return False

        # Creates and sends the command
        message = self.__get_new_command_message(self.UNSUBSCRIBE_SUBJECT_ADDRESS)
        for subject in subjects_set:
            self.__active_subscriptions.pop(subject)
            # TODO Usar? A remoção de callbacks tinha alguns problemas
            # self.__publication_receiver.receiver.removeCallback(subject, handler)
            message.add(subject)

        return self.__send_command(message)

    # endregion

    # region Command Receivers
    def __response_received(self, message, client):
        """
        :type message: Message
        :type client: ClientFactory
        """
        # Tests if the received message is an error
        if message.address == self.ERROR_ADDRESS:
            self.__print_error("Error command received.")
        else:
            log.msg("Response command received.")

        # Tries to get the command ID received in the message
        commandId = self.__get_command_id(message)
        if commandId is None:
            self.__print_error("Response received without a command identification number.")
            return

        # Tries to get the pending command associated to the received command ID
        command_handler_data = self.__pending_commands.get(commandId)
        if command_handler_data is None:
            self.__print_error("Received a command response with an unknown command identification number.")
            return

        # Redirects the message to the correspondent handler
        address = command_handler_data[1].address
        if address == self.REGISTER_USER_ADDRESS:
            self.__register_user_response_received(message, command_handler_data)
        elif address == self.REGISTER_SUBJECT_ADDRESS:
            self.__register_subject_response_received(message, command_handler_data)
        elif address == self.GET_AVAILABLE_SUBJECTS_ADDRESS:
            self.__get_available_subjects_response_received(message, command_handler_data)
        elif address == self.SUBSCRIBE_SUBJECT_ADDRESS:
            self.__subscribe_subject_response_received(message, command_handler_data)
        else:
            self.__print_error("Received an unknown response command (" + address + ").")

    def __register_user_response_received(self, message, command_handler_data):
        """
        :type message: Message
        :type command_handler_data: (int, Message, (bool) -> None)
        """
        was_successful = False

        # Tests if the received message is an response
        if message.address == self.RESPONSE_ADDRESS:
            # Tries to get the user ID received in the message
            user_id = self.__get_user_id(message)
            if user_id is None:
                self.__print_error("Invalid \"Register User\" response received. No OscUser ID found.")

            self.__osc_user_id = user_id
            was_successful = True

        # Removes the command from the pending list and calls the user's handler
        self.__pending_commands.pop(command_handler_data[0])
        self.__call_callback(command_handler_data[2], was_successful)

    def __register_subject_response_received(self, message, command_handler_data):
        """
        :type message: Message
        :type command_handler_data: (int, Message, (dict[str, bool]) -> None)
        """

        results = dict()
        # Tests if the number of subjects sent matchs the number of subjects received
        if len(message.arguments) != len(command_handler_data[1].arguments) - 1:
            self.__print_error("Invalid \"Register Subject\" response received. "
                               "The received message doesn't match the data sent.")
        else:
            for idx in range(1, len(message.arguments)):
                dataSent = command_handler_data[1].arguments[idx + 1].value
                dataReceived = message.arguments[idx].value
                if dataReceived is not None:
                    self.__active_subjects[dataSent] = dataReceived
                results[dataSent] = dataReceived is not None

        # Removes the command from the pending list and calls the user's handler
        self.__pending_commands.pop(command_handler_data[0])
        self.__call_callback(command_handler_data[2], results)

    def __get_available_subjects_response_received(self, message, command_handler_data):
        """
        :type message: Message
        :type command_handler_data: (int, Message, (list[str]) -> None)
        """

        results = []
        for idx in range(1, len(message.arguments)):
            results.append(message.arguments[idx].value)

        # Removes the command from the pending list and calls the user's handler
        self.__pending_commands.pop(command_handler_data[0])
        self.__call_callback(command_handler_data[2], results)

    def __subscribe_subject_response_received(self, message, command_handler_data):
        """
        :type message: Message
        :type command_handler_data: (int, Message, (dict[str, bool]) -> None)
        """

        results = dict()

        # Tests if the number of subjects sent matchs the number of subjects received
        if len(message.arguments) != (len(command_handler_data[1].arguments) + 1) / 3.:
            self.__print_error("Invalid \"Subscribe Subject\" response received. "
                               "The received message doesn't match the data sent.")
        else:
            rIdx = 1
            sIdx = 2
            # Tests if a subject was subscribed or not (received data is bool or null)
            while (rIdx < len(message.arguments)):

                subject = command_handler_data[1].arguments[sIdx].value  # type: str
                wasSubscribed = message.arguments[rIdx].value  # type: bool

                # Gets and removes the handler added when the command was sent
                handler = self.__pending_subscriptions[subject]
                self.__pending_subscriptions.pop(subject)

                if (wasSubscribed == True):
                    # Adds the subject and the received port to the active subjects list
                    self.__active_subscriptions[subject] = handler
                # TODO Usar? A remoção de callbacks tinha alguns problemas
                # self.__publication_receiver.receiver.addCallback(subject, handler)
                elif (command_handler_data[1].arguments[sIdx + 2].value == True):
                    self.__active_notifications.add(subject)

                results[subject] = wasSubscribed == True

                rIdx += 1
                sIdx += 3

        # Removes the command from the pending list and calls the user's handler
        self.__pending_commands.pop(command_handler_data[0])
        self.__call_callback(command_handler_data[2], results)

    def __subject_cancellation_received(self, message, client):
        """
        :type message: Message
        :type client: ClientFactory
        """
        if len(message.arguments) != 1:
            raise ValueError("An invalid subject cancellation alert was received")

        subject = message.arguments[0].value
        if (not isinstance(subject, str) or not regex_match(OscdClient.SUBJECT_REGEX_PATTERN, subject)):
            raise ValueError("An invalid subject notification alert was received")

        if self.__active_subscriptions.pop(subject, None) is not None:
            # TODO Usar? A remoção de callbacks tinha alguns problemas
            # self.__publication_receiver.receiver.removeCallback(subject, handler)
            if callable(self.__on_subject_cancellation_received):
                self.__call_callback(self.__on_subject_cancellation_received, subject)
            else:
                self.__command_fallback(message, client)

    def __subject_notification_received(self, message, client):
        """
        :type message: Message
        :type client: ClientFactory
        """
        if len(message.arguments) != 1:
            raise ValueError("An invalid subject notification alert was received")

        subject = message.arguments[0].value
        if (not isinstance(subject, str) or not regex_match(OscdClient.SUBJECT_REGEX_PATTERN, subject) or
                    subject not in self.__active_notifications):
            raise ValueError("An invalid subject notification alert was received")

        if subject not in self.__active_subscriptions:
            self.__active_notifications.remove(subject)
            if callable(self.__on_subject_notification_received):
                self.__call_callback(self.__on_subject_notification_received, subject)
            else:
                self.__command_fallback(message, client)

    def __command_fallback(self, message, client):
        """
        :type message: Message
        :type client: ClientFactory
        """
        # TODO Implementar
        log.msg("COMMAND FALLBACK.")
        pass

    # endregion

    def publish(self, subject, *data):
        """
        :type subject: str
        :type data: object
        :rtype: bool
        """

        # Tests if the client is registered
        if self.__osc_user_id is None:
            return False

        # Tests if the received subject is registered
        port = self.__active_subjects.get(subject, None)
        if (port is None):
            return False

        # Creates and sends the message
        message = Message(subject, self.__osc_user_id)
        for item in data:
            try:
                message.add(item)
            except OscError:
                raise ValueError("Some of the received data is unsupported.")

        if not self.is_connected:
            return False

        self.__publication_sender.send(message, (self.__address, port))
        return True

    def __publication_fallback(self, message, client):
        """
        :type message: Message
        :type client: ClientFactory
        """

        handler = self.__active_subscriptions.get(message.address, None)
        if (handler is None):
            self.__print_error("The received message has an unknown subject \"{0}\".".format(message.address))
        else:
            self.__call_callback(handler, message.arguments)

    def __call_callback(self, callback, *args):
        d = Deferred()
        d.addCallback(callback)
        d.addErrback(self.__callback_errback)
        d.callback(*args)

    def __callback_errback(self, failure):
        if callable(self.__on_callback_failure):
            self.__on_callback_failure(failure)
        else:
            log.err(failure, "Error raised while processing user's callback function.")

    # region Attributes Checkers
    def __has_register_user_command_pending(self):
        """
        :rtype: bool
        """
        for command in self.__pending_commands.values():
            if command[1].address == self.REGISTER_USER_ADDRESS:
                return True

        return False

    # endregion

    # region Information Getters
    @staticmethod
    def __get_command_id(message):
        """
        :type message: Message
        :rtype: int
        """
        if len(message.arguments) < 1 or not isinstance(message.arguments[0].value, int):
            return None
        else:
            return message.arguments[0].value

    @staticmethod
    def __get_user_id(message):
        """
        :type message: Message
        :rtype: str
        """
        if len(message.arguments) < 2 or not isinstance(message.arguments[1].value, str):
            return None

        user_id = message.arguments[1].value
        try:
            UUID(user_id)
        except ValueError:
            return None

        return user_id

    def __get_filtered_subjects(self, subjects, command_address):
        """
        :type subjects: tuple[str]
        :type command_address: str
        :rtype: set[str]
        """

        # Gets a set of non-repetitive and valid subjects
        subjects_set = self.__get_subject_set(subjects)

        # Excludes the received subjects that are already active or subscribed
        """:type:set"""
        subjects_set -= set(self.__active_subjects.keys())
        subjects_set -= set(self.__active_subscriptions.keys())
        subjects_set -= self.__active_notifications

        # Excludes the received subjects that are in pending commands
        self.__filter_subjects_by_pending_command(subjects_set, command_address)

        return subjects_set

    @staticmethod
    def __get_subject_set(subjects):
        """
        :type subjects: tuple[str]
        :rtype: set[str]
        """
        subjects_set = set(subjects)

        # Tests if any of the received subjects are repeated
        if len(subjects_set) < len(subjects):
            raise ValueError("Some of the received subjects are repeated.")

        # Tests if all the received subjects are valid
        if not all(isinstance(subject, str) and regex_match(OscdClient.SUBJECT_REGEX_PATTERN, subject)
                   for subject in subjects):
            raise ValueError("Some of the received subjects are not valid.")

        return subjects_set

    def __filter_subjects_by_pending_command(self, subjects, command_address):
        """
        :type subjects: set[str]
        :type command_address: str
        """
        for command_handler_data in self.__pending_commands.values():
            if command_handler_data[1].address == command_address:
                for message_argument in command_handler_data[1].arguments:
                    if message_argument.value in subjects:
                        subjects.discard(message_argument.value)

    # endregion

    @staticmethod
    def __print_error(message):
        """
        :type message: object
        """
        log.msg("ERROR:", message)


if __name__ == "__main__":
    raise NotImplementedError("This module can only be imported.")
