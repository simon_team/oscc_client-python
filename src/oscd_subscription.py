

class OscdSubscription:
	def __init__(self, subject, handler, enable_notification):
		"""
		:type subject: str
		:type handler: function[list[object]] | function[tuple[object]]
		:type enable_notification: bool
		"""
		if not (isinstance(subject, str) and len(subject) != 0 and callable(handler) and isinstance(enable_notification,
																									bool)):
			raise ValueError("Some of the parameters received are either wrong type or have invalid information")

		self.__subject = subject
		self.__handler = handler
		self.__enable_notification = enable_notification

	@property
	def subject(self):
		"""
		:rtype: str
		"""
		return self.__subject

	@property
	def handler(self):
		"""
		:rtype: function[list[object]] | function[tuple[object]]
		"""
		return self.__handler

	@property
	def enable_notification(self):
		"""
		:rtype: bool
		"""
		return self.__enable_notification
