import thread
import time
import random
import socket

from utils import *

from src.oscd_client import OscdClient
from src.oscd_subscription import OscdSubscription


# TODO Sincronismo das threads
class TesterClient(object):
	def __init__(self, address, port):
		"""
		:type address: str
		:type port: int
		"""
		self._address = address
		self._port = port
		self._client = OscdClient(address, port)
		self._client.set_on_subject_cancellation_received(self.__on_subject_cancellation_received)
		self._client.set_on_subject_notification_received(self.__on_subject_notification_received)
		self._client.set_disconnected(self.on_disconnected)

		self.__is_done = False

		self._subjects = dict()

	@property
	def is_done(self):
		return self.__is_done

	def start(self):
		if self.__is_done:
			raise Exception()

		self.__is_done = True
		self._start_tester()

	def _start_tester(self):
		raise NotImplementedError()

	def _connect(self):
		raw_input("\nPress 'ENTER' to connect.\n")
		print("Connect: {0}".format(self._client.connect(self.on_connected, True)))

	def on_connected(self, result):
		print("Connected: {0}".format(result))

	def _disconnect(self):
		raw_input("\nPress 'ENTER' to disconnect.\n")
		print("Disconnect: {0}".format(self._client.disconnect(self.on_disconnected, True)))

	def on_disconnected(self, result):
		print("Disconnected: {0}".format(result))

	def __on_subject_cancellation_received(self, subject):
		"""
		:type subject: str
		"""
		print_dark_green("The subject \"{0}\" was cancelled".format(subject))

	def __on_subject_notification_received(self, subject):
		"""
		:type subject: str
		"""
		print_dark_green("The subject \"{0}\" is now available".format(subject))

	def _on_register_user(self, data):
		"""
		:type data: bool
		"""
		begin_color(Color.DARK_CYAN, "(OnRegisterUser) Received data:")
		print("   [0]: {0}".format(data))
		end_color("-" * 20)

	# TODO Rever
	def _on_register_subject(self, data):
		"""
		:type data: dict
		"""
		begin_color(Color.DARK_CYAN, "(OnRegisterSubject) Received data:")
		for key in data.keys():
			print("   [{0}]: {1}".format(key, data[key]))
		end_color("-" * 20)

	# TODO Rever
	def _on_get_available_subjects(self, data):
		begin_color(Color.DARK_CYAN, "(OnGetAvailableSubjects) Received data:")
		for idx in range(len(data)):
			print("   [{0}]: {1}".format(idx, data[idx]))
		end_color("-" * 20)

	# TODO Rever
	def _on_subscribe_subject(self, data):
		begin_color(Color.DARK_CYAN, "(OnSubscribeSubject) Received data:")
		for key in data.keys():
			print("   [{0}]: {1}".format(key, data[key]))
		end_color("-" * 20)


class CommunicationTesterClient(TesterClient):
	def __init__(self, address, port):
		super(CommunicationTesterClient, self).__init__(address, port)

		self._is_stream_looping = True
		self._messages_sent = 0

	def on_connected(self, result):
		super(CommunicationTesterClient, self).on_connected(result)

		self._prepare_client()

	def _start_tester(self):
		raise NotImplementedError()

	def _prepare_client(self):
		raise NotImplementedError()

	def _stream(self):
		raw_input("\nPress 'ENTER' to start streaming.\n")

		thread.start_new_thread(self._start_stream, tuple([]))

	def _start_stream(self):
		raise NotImplementedError()

	def _disconnect(self):
		super(CommunicationTesterClient, self)._disconnect()
		self.stop()

	def stop(self):
		self._is_stream_looping = False


class CommandTesterClient1(TesterClient):
	def _start_tester(self):
		print("---===== OSCDClient Command Tester 1 =====---")

		self._subjects["C1S1"] = "/Client1/Subject1"
		self._subjects["C1S2"] = "/Client1/Subject2"
		self._subjects["C1S3"] = "/Client1/Subject3"
		self._subjects["C1S4"] = "/Client1/Subject4"
		self._subjects["C1S5"] = "/Client1/Subject5"
		self._subjects["C1S6"] = "/Client1/Subject6"
		self._subjects["C1S7"] = "/Client1/Subject7"

		self._subjects["C2S1"] = "/Client2/Subject1"
		self._subjects["C2S2"] = "/Client2/Subject2"
		self._subjects["C2S3"] = "/Client2/Subject3"
		self._subjects["C2S4"] = "/Client2/Subject4"

		self._connect()
		self.__register_user()
		self.__register_subjects_1()
		self.__get_available_subjects()
		self.__enable_subject_notifications()

		print("{0} Change Client {0}".format("-" * 50))

		self.__register_subjects_2()
		self.__unregister_subjects()

		print("{0} Change Client {0}".format("-" * 50))

		self._disconnect()

	def __register_user(self):
		raw_input("\nPress 'ENTER' to register the user. (2x)\n")
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

		raw_input("\nPress 'ENTER' to register the user.\n")
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def __register_subjects_1(self):
		raw_input("\nPress 'ENTER' to register the subjects \"{0}\" and \"{1}\". (2x)\n".format(
			self._subjects["C1S1"], self._subjects["C1S2"]))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S1"], self._subjects["C1S2"])))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S1"], self._subjects["C1S2"])))

		raw_input("\nPress 'ENTER' to register the subjects \"{0}\" and \"{1}\".\n".format(
			self._subjects["C1S1"], self._subjects["C1S2"]))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S1"], self._subjects["C1S2"])))

		raw_input("\nPress 'ENTER' to register the subjects \"{0}\", \"{1}\", \"{2}\" and \"{3}\".\n".format(
			self._subjects["C1S1"], self._subjects["C1S2"], self._subjects["C1S3"], self._subjects["C1S4"]))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S1"], self._subjects["C1S2"], self._subjects["C1S3"],
			self._subjects["C1S4"])))

	def __get_available_subjects(self):
		raw_input("\nPress 'ENTER' to get the available subjects. (2x)\n")
		print("GetAvailableSubjects: {0}".format(self._client.get_available_subjects(self._on_get_available_subjects)))
		print("GetAvailableSubjects: {0}".format(self._client.register_user(self._on_get_available_subjects)))

	def __enable_subject_notifications(self):
		raw_input(
			"\nPress 'ENTER' to enable notifications to the subjects \"{0}\", \"{1}\" and \"{2}\".\n".format(
				self._subjects["C1S1"], self._subjects["C1S2"], self._subjects["C1S3"]))
		print("EnableSubjectNotification: {0}".format(self._client.enable_subject_notification(
			self._subjects["C1S1"], self._subjects["C1S2"], self._subjects["C1S3"])))

		raw_input(
			"\nPress 'ENTER' to enable notifications to the subjects \"{0}\", \"{1}\" and \"{2}\".\n".format(
				self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"]))
		print("EnableSubjectNotification: {0}".format(self._client.enable_subject_notification(
			self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"])))

		raw_input(
			"\nPress 'ENTER' to enable notifications to the subjects \"{0}\", \"{1}\" and \"{2}\".\n".format(
				self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"]))
		print("EnableSubjectNotification: {0}".format(self._client.enable_subject_notification(
			self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"])))

		raw_input(
			"\nPress 'ENTER' to enable notifications to the subjects \"{0}\", \"{1}\", \"{2}\" and \"{3}\".\n".format(
				self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"], self._subjects["C2S4"]))
		print("EnableSubjectNotification: {0}".format(self._client.enable_subject_notification(
			self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"], self._subjects["C2S4"])))

	def __register_subjects_2(self):
		raw_input("\nPress 'ENTER' to register the subjects \"{0}\", \"{1}\" and \"{2}\".\n".format(
			self._subjects["C1S5"], self._subjects["C1S6"], self._subjects["C1S7"]))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S5"], self._subjects["C1S6"], self._subjects["C1S7"])))

	def __unregister_subjects(self):
		raw_input("\nPress 'ENTER' to unregister the subject \"{0}\".\n".format(self._subjects["C1S2"]))
		print("UnregisterSubject: {0}".format(self._client.unregister_subject(self._subjects["C1S2"])))

		raw_input("\nPress 'ENTER' to unregister the subject \"{0}\".\n".format(self._subjects["C1S2"]))
		print("UnregisterSubject: {0}".format(self._client.unregister_subject(self._subjects["C1S2"])))


class CommandTesterClient2(TesterClient):
	def __init__(self, address, port):
		super(CommandTesterClient2, self).__init__(address, port)

		self.__subscription1_counter = 0
		self.__subscription2_counter = 0
		self.__subscription3_counter = 0

	def _start_tester(self):
		print("---===== OSCDClient Command Tester 2 =====---")

		self._subjects["C1S1"] = "/Client1/Subject1"
		self._subjects["C1S2"] = "/Client1/Subject2"
		self._subjects["C1S3"] = "/Client1/Subject3"
		self._subjects["C1S4"] = "/Client1/Subject4"
		self._subjects["C1S5"] = "/Client1/Subject5"
		self._subjects["C1S6"] = "/Client1/Subject6"
		self._subjects["C1S7"] = "/Client1/Subject7"

		self._subjects["C2S1"] = "/Client2/Subject1"
		self._subjects["C2S2"] = "/Client2/Subject2"
		self._subjects["C2S3"] = "/Client2/Subject3"
		self._subjects["C2S4"] = "/Client2/Subject4"

		self._connect()
		self.__register_user()
		self.__register_subjects()
		self.__subscribe_subjects_1()
		self.__enable_subject_notifications()
		self.__disable_subject_notifications()

		print("{0} Change Client {0}".format("-" * 50))

		self.__unsubscribe_subjects()
		self.__subscribe_subjects_2()

		print("{0} Change Client {0}".format("-" * 50))

		self._disconnect()

	def __register_user(self):
		raw_input("\nPress 'ENTER' to register the user.\n")
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def __register_subjects(self):
		raw_input("\nPress 'ENTER' to register the subjects \"{0}\", \"{1}\", \"{2}\" and \"{3}\".\n".format(
			self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"], self._subjects["C2S4"]))
		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C2S1"], self._subjects["C2S2"], self._subjects["C2S3"],
			self._subjects["C2S4"])))

	def __subscribe_subjects_1(self):
		c1_subscription1 = OscdSubscription(self._subjects["C1S1"], self.__on_subscription_received1, False)
		c1_subscription2 = OscdSubscription(self._subjects["C1S2"], self.__on_subscription_received2, False)
		c1_subscription5 = OscdSubscription(self._subjects["C1S5"], self.__on_subscription_received3, True)
		c1_subscription6 = OscdSubscription(self._subjects["C1S6"], self.__on_subscription_received3, True)
		c1_subscription7 = OscdSubscription(self._subjects["C1S7"], self.__on_subscription_received3, False)

		raw_input("\nPress 'ENTER' to subscribe subjects \"{0}\", \"{1}\", \"{2}\", \"{3}\" and \"{4}\".\n".format(
			self._subjects["C1S1"], self._subjects["C1S2"], self._subjects["C1S5"],
			self._subjects["C1S6"], self._subjects["C1S7"]))
		print("SubscribeSubject: {0}".format(self._client.subscribe_subject(
			self._on_subscribe_subject, c1_subscription1, c1_subscription2, c1_subscription5, c1_subscription6,
			c1_subscription7)))

	def __enable_subject_notifications(self):
		raw_input(
			"\nPress 'ENTER' to enable notifications to the subjects \"{0}\" and \"{1}\".\n".format(
				self._subjects["C1S3"], self._subjects["C1S4"]))
		print("EnableSubjectNotification: {0}".format(self._client.enable_subject_notification(
			self._subjects["C1S3"], self._subjects["C1S4"])))

	def __disable_subject_notifications(self):
		raw_input(
			"\nPress 'ENTER' to disable notifications to the subjects \"{0}\" and \"{1}\". (2x)\n".format(
				self._subjects["C1S6"], self._subjects["C1S7"]))
		print("DisableSubjectNotification: {0}".format(self._client.disable_subject_notification(
			self._subjects["C1S6"], self._subjects["C1S7"])))
		print("DisableSubjectNotification: {0}".format(self._client.disable_subject_notification(
			self._subjects["C1S6"], self._subjects["C1S7"])))

		raw_input("\nPress 'ENTER' to disable notifications to the subjects \"{0}\".\n".format(self._subjects["C1S6"]))
		print(
			"DisableSubjectNotification: {0}".format(self._client.disable_subject_notification(self._subjects["C1S6"])))

	def __unsubscribe_subjects(self):
		raw_input("\nPress 'ENTER' to unsubscribe the subject \"{0}\". (2x)\n".format(self._subjects["C1S1"]))
		print("UnsubscribeSubject: {0}".format(self._client.unsubscribe_subject(self._subjects["C1S1"])))
		print("UnsubscribeSubject: {0}".format(self._client.unsubscribe_subject(self._subjects["C1S1"])))

		raw_input("\nPress 'ENTER' to unsubscribe the subject \"{0}\". (2x)\n".format(self._subjects["C1S1"]))
		print("UnsubscribeSubject: {0}".format(self._client.unsubscribe_subject(self._subjects["C1S1"])))

	def __subscribe_subjects_2(self):
		c1_subscription1 = OscdSubscription(self._subjects["C1S1"], self.__on_subscription_received1, False)

		raw_input("\nPress 'ENTER' to subscribe the subject \"{0}\".\n".format(self._subjects["C1S1"]))
		print(
			"SubscribeSubject: {0}".format(
				self._client.subscribe_subject(self._on_subscribe_subject, c1_subscription1)))

		raw_input("\nPress 'ENTER' to subscribe the subject \"{0}\".\n".format(self._subjects["C1S1"]))
		print(
			"SubscribeSubject: {0}".format(
				self._client.subscribe_subject(self._on_subscribe_subject, c1_subscription1)))

	def __on_subscription_received1(self, data):
		print_dark_magenta("({0}) 1: Received message with {1} elements".format(
			self.__subscription1_counter, len(data)))

	def __on_subscription_received2(self, data):
		print_dark_magenta("({0}) 2: Received message with {1} elements".format(
			self.__subscription2_counter, len(data)))

	def __on_subscription_received3(self, data):
		print_dark_magenta("({0}) 3: Received message with {1} elements".format(
			self.__subscription3_counter, len(data)))


class CommunicationTesterClient1(CommunicationTesterClient):
	def __init__(self, address, port):
		super(CommunicationTesterClient1, self).__init__(address, port)

		self.__stream1_message_counter = 0
		self.__stream2_message_counter = 0

		self.__subscription1_counter = 0
		self.__subscription2_counter = 0

	def _start_tester(self):
		print("---===== OSCDClient Communication Tester 1 =====---")

		self._subjects["C1S1"] = "/Client1/Subject1"
		self._subjects["C1S2"] = "/Client1/Subject2"

		self._subjects["C2S1"] = "/Client2/Subject1"
		self._subjects["C3S1"] = "/Client3/Subject1"

		self._connect()

		self._stream()

		self._disconnect()

	def _prepare_client(self):
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def _on_register_user(self, data):
		super(CommunicationTesterClient1, self)._on_register_user(data)

		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C1S1"], self._subjects["C1S2"])))

	def _on_register_subject(self, data):
		super(CommunicationTesterClient1, self)._on_register_subject(data)

		c2_subscription1 = OscdSubscription(self._subjects["C2S1"], self.__on_subscription_received1, False)
		c3_subscription1 = OscdSubscription(self._subjects["C3S1"], self.__on_subscription_received2, False)

		print("SubscribeSubject: {0}".format(self._client.subscribe_subject(
			self._on_subscribe_subject, c2_subscription1, c3_subscription1)))

	def __on_subscription_received1(self, data):
		self.__subscription1_counter += 1
		print_dark_magenta("(1 Received)   ({0} | {1})".format(
			self.__subscription1_counter, self.__subscription2_counter))

	def __on_subscription_received2(self, data):
		self.__subscription2_counter += 1
		print_dark_magenta("(2 Received)   ({0} | {1})".format(
			self.__subscription1_counter, self.__subscription2_counter))

	def _start_stream(self):
		subject1 = self._subjects["C1S1"]
		subject2 = self._subjects["C1S2"]
		while self._is_stream_looping:
			if random.random() > .5:
				if self._client.publish(subject1, random.random(), random.randint(0, 1000000), random.random() > .5,
										subject1):
					self.__stream1_message_counter += 1
					print("(1 Sent - {0}) ({1} | {2})".format(
						subject1, self.__stream1_message_counter, self.__stream2_message_counter))
				else:
					print("(1 Error) ({0} | {1})".format(
						self.__stream1_message_counter, self.__stream2_message_counter))
			else:
				if self._client.publish(subject2, random.random(), random.randint(0, 1000000), random.random() > .5,
										subject2):
					self.__stream2_message_counter += 1
					print("(2 Sent - {0}) ({1} | {2})".format(
						subject2, self.__stream1_message_counter, self.__stream2_message_counter))
				else:
					print("(2 Error) ({0} | {1})".format(
						self.__stream1_message_counter, self.__stream2_message_counter))

			time.sleep(1)


class CommunicationTesterClient2(CommunicationTesterClient):
	def __init__(self, address, port):
		super(CommunicationTesterClient2, self).__init__(address, port)

		self.__stream_message_counter = 0

	def _start_tester(self):
		print("---===== OSCDClient Communication Tester 2 =====---")

		self._subjects["C2S1"] = "/Client2/Subject1"

		self._connect()

		self._stream()

		self._disconnect()

	def _prepare_client(self):
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def _on_register_user(self, data):
		super(CommunicationTesterClient2, self)._on_register_user(data)

		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C2S1"])))

	def _start_stream(self):
		subject = self._subjects["C2S1"]
		while self._is_stream_looping:
			if self._client.publish(subject, random.random(), random.randint(0, 1000000), random.random() > .5,
									subject):
				self.__stream_message_counter += 1
				print("(2 Sent) ({0})".format(self.__stream_message_counter))
			else:
				print("(2 Error) ({0})".format(self.__stream_message_counter))


class CommunicationTesterClient3(CommunicationTesterClient):
	def __init__(self, address, port):
		super(CommunicationTesterClient3, self).__init__(address, port)

		self.__stream_message_counter = 0

	def _start_tester(self):
		print("---===== OSCDClient Communication Tester 3 =====---")

		self._subjects["C3S1"] = "/Client3/Subject1"

		self._connect()

		self._stream()

		self._disconnect()

	def _prepare_client(self):
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def _on_register_user(self, data):
		super(CommunicationTesterClient3, self)._on_register_user(data)

		print("RegisterSubject: {0}".format(self._client.register_subject(
			self._on_register_subject, self._subjects["C3S1"])))

	def _start_stream(self):
		subject = self._subjects["C3S1"]
		while self._is_stream_looping:
			if self._client.publish(subject, random.random(), random.randint(0, 1000000), random.random() > .5,
									subject):
				self.__stream_message_counter += 1
				print("(3 Sent) ({0})".format(self.__stream_message_counter))
			else:
				print("(3 Error) ({0})".format(self.__stream_message_counter))


class CommunicationTesterClient4(CommunicationTesterClient):
	def __init__(self, address, port):
		super(CommunicationTesterClient4, self).__init__(address, port)

		self.__subscription_counter = 0

	def _start_tester(self):
		print("---===== OSCDClient Communication Tester 4 =====---")

		self._subjects["C1S1"] = "/Client1/Subject1"
		self._subjects["C1S2"] = "/Client1/Subject2"

		self._connect()

		self._disconnect()

	def _prepare_client(self):
		print("RegisterUser: {0}".format(self._client.register_user(self._on_register_user)))

	def _on_register_user(self, data):
		super(CommunicationTesterClient4, self)._on_register_user(data)

		c1_subscription1 = OscdSubscription(self._subjects["C1S1"], self.__on_subscription_received, False)
		c1_subscription2 = OscdSubscription(self._subjects["C1S2"], self.__on_subscription_received, False)

		print("SubscribeSubject: {0}".format(self._client.subscribe_subject(
			self._on_subscribe_subject, c1_subscription1, c1_subscription2)))

	def __on_subscription_received(self, data):
		self.__subscription_counter += 1
		print_dark_magenta("(Received)   ({0})".format(self.__subscription_counter))

	def _start_stream(self):
		pass


def ask_oscd_address():
	n_iter = 0
	received_string = ""
	while received_string == "":
		cls()
		if n_iter > 0:
			print_light_red("Invalid Option...")
		received_string = raw_input("Enter the OSC Distributor IP address or 'ENTER' to loopback: ")

		if len(received_string) == 0:
			return "127.0.0.1"

		try:
			socket.inet_aton(received_string)
		except socket.error:
			received_string = ""

		n_iter += 1
	return received_string


def write_menu(address):
	"""
	:type address: str
	:rtype int:
	"""
	n_iter = 0
	value = -1
	while not (0 < value < 7):
		cls()
		print("OSC Distributor IP: " + address)
		print("+-------------------------------------------------------------------------+")
		print("|                                                                         |")
		print("|                  OSC Distributor Python Client Tester                   |")
		print("|                                                                         |")
		print("| 1: Command Tester Client 1                                              |")
		print("|    - Registers 4 subjects                                               |")
		print("|    - Gets the available subjects                                        |")
		print("|    - Enables notification for 3 (self) subjects                         |")
		print("|    - Enables notification for 4 subjects of Client 2                    |")
		print("|    - Registers 3 subjects                                               |")
		print("|    - Unregisters 1 subject                                              |")
		print("| 2: Command Tester Client 2                                              |")
		print("|    - Registers 4 subjects                                               |")
		print("|    - Subscribes to 5 subjects of Client 1                               |")
		print("|    - Enables notification for 2 subjects of Client 1                    |")
		print("|    - Disables notification for 2 subjects of Client 1                   |")
		print("|    - Unsubscribes to 1 subjects of Client 1                             |")
		print("|    - Subscribes to 1 subjects of Client 1                               |")
		print("| 3: Communication Tester Client 1                                        |")
		print("|    - Registers 2 subjects                                               |")
		print("|    - Subscribes to the subjects of Client 2 and Client 3                |")
		print("|    - Publishs in one of them every 1 second                             |")
		print("| 4: Communication Tester Client 2                                        |")
		print("|    - Registers 1 subject                                                |")
		print("|    - Publishs on it as fast as possible                                 |")
		print("| 5: Communication Tester Client 3                                        |")
		print("|    - Registers 1 subject                                                |")
		print("|    - Publishs on it as fast as possible                                 |")
		print("| 6: Communication Tester Client 4                                        |")
		print("|    - Subscribes to the subjects of Client 1                             |")
		print("+-------------------------------------------------------------------------+")

		if n_iter > 0:
			print_light_red("Invalid Option...")
		received_string = raw_input("Select the client to use: ")

		try:
			value = int(received_string)
		except ValueError:
			value = -1

		n_iter += 1

	return value


if __name__ == "__main__":
	oscd_address = ask_oscd_address()
	oscd_port = 5103
	menu_value = write_menu(oscd_address)
	oscd_client = None

	if menu_value == 1:
		oscd_client = CommandTesterClient1(oscd_address, oscd_port)
	elif menu_value == 2:
		oscd_client = CommandTesterClient2(oscd_address, oscd_port)
	elif menu_value == 3:
		oscd_client = CommunicationTesterClient1(oscd_address, oscd_port)
	elif menu_value == 4:
		oscd_client = CommunicationTesterClient2(oscd_address, oscd_port)
	elif menu_value == 5:
		oscd_client = CommunicationTesterClient3(oscd_address, oscd_port)
	elif menu_value == 6:
		oscd_client = CommunicationTesterClient4(oscd_address, oscd_port)

	if oscd_client is not None:
		oscd_client.start()

	raw_input("\nPress 'ENTER' to exit.")
